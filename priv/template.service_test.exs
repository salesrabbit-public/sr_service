defmodule <MODULE_NAME>Test do
  @moduledoc false
  use ExUnit.Case
  doctest <MODULE_NAME>

  test "greets the world" do
    assert <MODULE_NAME>.hello(nil) == "Hello!"
  end

  test "greets a specific person" do
    assert <MODULE_NAME>.hello("My Friend", %{account_id: 1, user_id: 2}) ==
             "Hello My Friend from account 1!"
  end
end
