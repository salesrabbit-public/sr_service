# <MODULE_NAME>

This app was auto generated with the SalesRabbit Service Generator and may be used in the SalesRabbit EcoSystem.

# Next Steps

- Add this code to a Gitlab Repo under the [SalesRabbit/services](https://gitlab.com/groups/salesrabbit/services) project
- Contact the infrastructure team to set up deploy files (value and secrets) and a DB if this project requires one.
- Have fun developing your app!

When you are ready to deploy your application you need to add your repo to the config in the InternalServices project. 
 TODO: Need to set up the Internal Services repo/project

# Docs

You may view the docs [here](https://salesrabbit.gitlab.io/spikes/<APP_NAME>) once the first deploy is complete.

# Environment Variables

- APPSIGNAL_APP_ENV
- APPSIGNAL_APP_NAME
- APPSIGNAL_OTP_APP
- APPSIGNAL_PUSH_API_KEY
- DATABASE_URL (required)
- ERLANG_COOKIE (required, must be the same as other applications you want to cluster with)
- LOG_LEVEL
- POD_IP (required)

# Project 
This project is to be used in the SalesRabbit EcoSystem. It will be clustered with other applications and must have a well defined and documented public interface. The public interface for this application can be found in the `lib/<APP_NAME>.ex` file. 

Only public functions should exist in the main file and they are required to have up to date documentation as well as typespecs. 

## Code Quality

To run all code quality checks you can use the `mix audit` command.
Addtional elixir built-in checks are run in the CI piplines. Feel free to take a look in the `gitlab-ci.yml` file.

### Security Auditing
This project uses [MixAudit](https://hexdocs.pm/mix_audit/readme.html) for security auditing. 
Run `mix hex.audit` or `mix deps.audit` to view a report.

### Static Analysis
This project uses [Credo](https://hexdocs.pm/credo/overview.html) for static code analysis.
Run `mix credo --strict` to view a report.


## Telemetry
This project uses [AppSignal](https://www.appsignal.com/) for APM and telemetry.
You may view their documentation [here](https://docs.appsignal.com/)

## Database
This project is setup to use [Ecto](https://hexdocs.pm/ecto/Ecto.html) for it's database connection and schema management along with the [Postgres](https://www.postgresql.org/) database and the [postgrex](https://hexdocs.pm/postgrex/readme.html) adapter for ecto.



