defmodule <MODULE_NAME> do
  @moduledoc """
  This is the public interface for <MODULE_NAME>

  We can access all of <MODULE_NAME> domain objects as well as perform crud operations and search functions.

  Each function takes a context as the last argument so we can keep our data scoped properly.

  We have good documentation and type specs so we can get better error messages and code hints as well as automatic cient generation.

  You can view possible layouts for docs here https://hexdocs.pm/ex_doc/cheatsheet.html

  You can view typespec docs here https://hexdocs.pm/elixir/1.14/typespecs.html#content
	"""

  @doc """
  Say "Hello" to my little friend.

  ```elixir
  <MODULE_NAME>.hello("my little friend", %{account_id: 1, user_id: 2})
  "Hello my little friend from account 1!"
  ```
  """
  @spec hello(String.t(), %{:account_id => :integer, :user_id => :integer, optional(any) => any}) :: String.t()
  def hello(name, context) do
    %{account_id: account_id} = context
    "Hello #{name} from account #{account_id}!"
  end

  @doc """
  Say "Hello" to anyone.

  ```elixir
  <MODULE_NAME>.hello(%{account_id: 1, user_id: 2})
  "Hello"
  ```
  """
  @spec hello(%{optional(any) => any}) :: String.t()
  def hello(_context) do
    "Hello!"
  end
end
