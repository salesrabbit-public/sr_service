import Config

config :logger,
  backends: [:console]

config :logger, :console,
  level: String.to_atom(System.get_env("LOG_LEVEL", "warn")),
  format: "$date $time $metadata[$level] $message\n",
  metadata: [:request_id, :file, :line, :mfa]

  config :<APP_NAME>, <MODULE_NAME>.Repo,
    url: System.get_env("DATABASE_URL", "no-database")
