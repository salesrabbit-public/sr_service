defmodule SrServiceTest do
  use ExUnit.Case
  doctest SrService

  test "write_log_file" do
    file_dir = File.cwd!() <> "/" <> "test/temp"
    File.mkdir(file_dir
    File.mkdir(file_dir <> "/lib")
    assert SrService.write_log_file(file_dir, "temp") == :ok
  end
end
