defmodule SrService.MixProject do
  use Mix.Project

  def project do
    [
      app: :sr_service,
      organization: "salesrabbit",
      version: "0.1.10",
      elixir: "~> 1.14",
      start_permanent: Mix.env() == :prod,
      description: description(),
      package: package(),
      source_url: "https://gitlab.com/salesrabbit-public/sr_service",
      name: "SalesRabbit Service Generator",
      deps: deps()
    ]
  end

  # Run "mix help compile.app" to learn about applications.
  def application do
    []
  end

  # Run "mix help deps" to learn about dependencies.
  defp deps do
    [
      {:ex_doc, "~> 0.29", only: :dev, runtime: false}
    ]
  end

  defp description() do
    """
    The SalesRabbit Service Generator is a package for generating new service applications inside the SalesRabbit ecosystem.
    """
  end

  defp package() do
    [
      files: ~w(lib priv .tool-versions .gitignore .formatter.exs mix.exs README* LICENSE*),
      licenses: ["MIT"],
      links: %{
        "GitLab" => "https://gitlab.com/salesrabbit-public/sr_service"
      }
    ]
  end
end
