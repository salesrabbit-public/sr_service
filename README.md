# SrService

This is the project generator for new services at SalesRabbit.

## Installation

In a terminal run: `mix archive.install hex sr_service`

## Usage

In a terminal navigate to the directory you want the new project to be generated. 

Run: `mix sr_service.new <new_app>` 

A new project will be generated in the `<new_app>` folder.


## Development

When editing templates use `<APP_NAME>` to variablize the application name in the template.

To build this app and test locally run:

```
$> mix archive.build
$> mix archive.install <file path from above results>
$> mix sr_service.new <my_new_app>
```

